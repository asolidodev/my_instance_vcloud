data "template_file" "my_instance_install_puppet_node" {
  # Workaround that a variable default cannot have interpolation
  template = file(var.my_instance_initscript == "" ? local.initscript : var.my_instance_initscript)
  vars = {
    master_fqdn = local.puppet_master_fqdn_parsed
    master_ip   = var.my_instance_puppet_master_ip
    nodename    = local.my_prefix_naming
    domain      = var.my_instance_domain_name
    environment = var.my_instance_puppet_environment
  }
}

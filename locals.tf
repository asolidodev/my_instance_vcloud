locals {
  # let's build our prefix for naming things
  my_prefix_naming = terraform.workspace == "default" ? join("-", list(var.my_instance_name_prefix, var.my_instance_name)) : join("-", list(var.my_instance_name_prefix, terraform.workspace, var.my_instance_name))
  # Let's use a local to get a parsed puppet master FQDN
  # which uses puppet.domain_name as default if
  # the puppet master FQDN is not given.
  # This allows this module to be invoked even if the var is not set
  # as was done until recently.
  puppet_master_fqdn_parsed = var.my_instance_puppet_master_fqdn == "" ? join(".", list("puppet", var.my_instance_domain_name)) : var.my_instance_puppet_master_fqdn
  initscript                = "${path.module}/install-puppet-node.sh.tpl"
}

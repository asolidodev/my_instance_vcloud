terraform {
  required_providers {
    template = {
      source = "hashicorp/template"
    }
    vcd = {
      source = "vmware/vcd"
    }
  }
  required_version = "~> 0.13"
}

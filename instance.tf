resource "vcd_vapp_vm" "my_instance" {
  vapp_name       = var.my_instance_vapp_name
  name            = local.my_prefix_naming
  catalog_name    = var.my_instance_catalog
  template_name   = var.my_instance_template_name
  memory          = var.my_instance_memory
  cpus            = var.my_instance_cpus
  cpu_cores       = var.my_instance_cpu_cores
  storage_profile = var.my_instance_storage_profile

  metadata = {
    role = var.my_instance_name
  }

  guest_properties = {
    "guest.hostname" = local.my_prefix_naming
  }

  dynamic "network" {
    for_each = var.my_instance_nics

    content {
      type               = lookup(network.value, "type", var.my_instance_network_type)
      name               = lookup(network.value, "name", var.my_instance_network_name)
      ip_allocation_mode = lookup(network.value, "ip_allocation_mode", var.my_instance_network_ip_allocation_mode)
      ip                 = lookup(network.value, "ip", "")
    }
  }

  dynamic "disk" {
    # We need this validation check in case no my_instance_disks is given to the module, so it won't
    # lookup for values out of bounds
    for_each = length(var.my_instance_disks) == 0 ? [] : var.my_instance_disks

    content {
      name        = lookup(disk.value, "name", null)
      bus_number  = lookup(disk.value, "bus_number", null)
      unit_number = lookup(disk.value, "unit_number", null)
    }
  }

  customization {
    # WARNING: true value will cause the VM to reboot on every apply operation.
    force = var.my_instance_deploying
    # will enable guest customization which may occur on first boot or if the force flag is used.
    enabled    = true
    initscript = data.template_file.my_instance_install_puppet_node.rendered
  }

  # we need this to avoid instance recreation on multi vCloud management
  lifecycle {
    ignore_changes = [org, vdc, template_name]
  }

}

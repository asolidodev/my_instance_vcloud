variable my_instance_name_prefix {
  type        = string
  description = "A string of names to add to the instance name and hostname to identify the organization, environment, sub-environment, etc. For example, 'bigclient-production-us'. If the terraform workspace is not 'default', the workspace gets appended to this prefix. For example for a 'test' workspace you get 'bigclient-production-us-test-'"
}
variable my_instance_name {
  type        = string
  description = "The name used in the instance name (and hostname). For example, 'gw' for a gateway or 'lb' for a load balancer. The my_instance_name_prefix is prepended (including any non-default workspace name). A number is appended."
}
variable my_instance_domain_name {
  type        = string
  description = "The domain_name to be used, e.g. example.com, that gets appended to the hostname (dot separated)."
}
variable my_instance_initscript {
  type    = string
  default = ""
}
variable my_instance_nics {
  # Map structure:
  # [{
  #       "type": "",
  #       "name": "",
  #       "ip_allocation_mode": "",
  #       "ip": ""
  #  }]
  type        = list(map(string))
  description = "A list (of maps) with the configurations for the instance nics."
}
variable my_instance_puppet_master_fqdn {
  type        = string
  description = "The fqdn of the instance's puppet master. If an empty string is used here (the default) then puppet.domain_name is used."
  default     = ""
}
variable my_instance_puppet_master_ip {
  type        = string
  description = "The ip of the instance's puppet master."
}
variable my_instance_puppet_environment {
  type        = string
  description = "The puppet environment (e.g. 'production' or 'staging'). We need this for the puppet node configuration."
}
variable my_instance_template_file_install_puppet_node {
  type        = string
  description = "A local filepath to use for the template to install puppet on instance launch (instead of the module's default template)"
  default     = ""
}
variable my_instance_deploying {
  default = false
}
variable my_instance_vapp_name {
}
variable my_instance_catalog {
}
variable my_instance_template_name {
}
variable my_instance_memory {
  type = string
}
variable my_instance_cpus {
  type = string
}
variable my_instance_cpu_cores {
  type = string
}
variable my_instance_storage_profile {
  default = ""
}
variable my_instance_network_type {
  type = string
}
variable my_instance_network_name {
  type = string
}
variable my_instance_network_ip_allocation_mode {
  type = string
}
# Disks
variable my_instance_disks {
  # Map structure:
  # [
  #   {
  #     name = "",
  #     bus_number = "",
  #     unit_number = "",
  #   }
  # ]
  # Instances without disks should have an empty list. ex:
  #            [ [], [{name = "second_instance_disk_1", bus_number = "0", unit_number = "1"}]]
  #
  # Note how the first element of the root list is an empty list. This element represents the
  # external disks for the first instance. In this case, no disks are present.
  type        = list(map(string))
  description = "A list of lists(of maps) with the configurations for the instances disks to add."
  default     = []
}

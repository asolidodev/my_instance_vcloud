#!/bin/bash
# usage example: sudo ./install-puppet-node.sh 'fqdn'

SETHOSTNAME=yes
START=yes
RELEASE=`lsb_release -cs`
REPOPKG=/tmp/puppetlabs-release-$RELEASE.deb
REPOPKGURL=http://apt.puppetlabs.com/puppetlabs-release-$RELEASE.deb
NODENAME=${nodename}
MASTER=${master_fqdn}
MASTERIP=${master_ip}
ENVIRONMENT=${environment}

#### main ####

if [ -z $NODENAME ]; then
 echo usage: $0 NODENAME
 exit
fi

# set hostname
if [ x$SETHOSTNAME = 'xyes' ]; then
 hostnamectl set-hostname  $NODENAME
 sed -i "s/127.0.0.1 localhost/127.0.0.1 $NODENAME localhost/" /etc/hosts
fi

apt-get update
apt-get install -y wget
wget -O $REPOPKG $REPOPKGURL
dpkg -i $REPOPKG
apt-get update
apt-get install -y ntp
apt-get install -y puppet

# add puppet master ip to hosts

if [ ! -z $MASTERIP ]; then
  echo "$MASTERIP $MASTER" >> /etc/hosts
fi

# configurations  and start puppet service
cp -f /etc/puppet/puppet.conf /etc/puppet/puppet.conf.orig
sed -i "s/\[main\]/\[main\]\nserver=$MASTER\nenvironment=$ENVIRONMENT\nnode_name=cert\ncertname=$NODENAME/" /etc/puppet/puppet.conf

# fix default conf line that brings up warnings
sed -i 's/templatedir=$confdir\/templates//' /etc/puppet/puppet.conf

puppet resource service puppet ensure=running enable=true
puppet agent -t

if [ x$START = 'xyes' ]; then
  sed -i 's/START=no/START=yes/' /etc/default/puppet
  service puppet start
fi
